![Captura de Pantalla](img/logo_viar.png)

# Una herramienta para la estimación de la función de riesgo.

[![](https://img.shields.io/badge/Shiny-shinyapps.io-blue?style=flat&labelColor=white&logo=RStudio&logoColor=blue)]( https://diegoarare.shinyapps.io/VIAR/)

El estudio de métodos para analizar datos sobre eventos observados a lo largo del tiempo y de factores asociados con la tasa de ocurrencia de estos eventos, es un tema que siempre ha preocupado, sobre todo en el ámbito de la salud. De esta forma, el análisis se le conoce genéricamente como ``Análisis de supervivencia''. Si bien por lo general se hace énfasis en la función de supervivencia en este tipo de análisis, poder estimar de la mejor manera la tasa de riesgo instantánea asociada a alguna enfermedad hasta el fallo, remisión o recurrencia de la misma, es también un tema relevante.

``VIAR'' - como se le ha llamado a la herramienta - pretende ser un aplicación interactiva, con la cuál se puede realizar el análisis de supervivencia (en particular de la función de riesgo univariada y condicional) de una manera bastante simple, evitando al usuario tener que lidiar con el código necesario para hacerlo. Esta herramienta se basa en parte de la metodología propuesta por autores de la Universidad de Mazaryk.

Se puede acceder a la misma a través del siguiente [**link**](https://diegoarare.shinyapps.io/VIAR/). 


## Como cargar la base de datos?

![Captura de Pantalla](img/manual-carga-gitlab.png)

El mecanismo de carga consta de los siguientes pasos, que hay que seguir de forma secuencial para que quede todo correctamente cargado:

* Cargamos el archivo que queremos analizar en (5), lo cuál apenas cargue se podrán ver los datos reflejados en (7), en formato de tabla.

* En (6) tenemos varias opciones para modificar la estructura de datos cargados, es decir si las columnas tienen título o no, y cual es el separador de los datos. Esto último es importante, dado que si no ingresamos correctamente el separador, los datos van a quedar ingresados únicamente en una columna, lo cual es incorrecto. La forma correcta es que cada variable o campo ocupe una columna como se puede apreciar en la imagen. Todos los cambios que hagamos aquí se ven reflejados en (7).

* En (8) debemos seleccionar, de todos los datos, los específicos de cualquier análisis de supervivencia, es decir, un tiempo hasta que ocurre un evento o no, el estado (si ocurre el evento o si es censurado) y covariables de interés para extender el análisis. La aplicación acepta hasta cuatro covariables, siendo una de ellas la principal y las otras tres auxiliares (dos numéricas y una categórica). La covariable principal, si es categórica, es necesario seleccionar la opción *``Variable categórica''*, ya que si no se realiza esto, puede dar visualizaciones erróneas.	
		
Otro tema relevante a comentar en este punto, es que el **estado solo toma valores binarios 0-1**, es decir:
			
    * Valor 0: Dato censurado
    * Valor 1: Dato con presencia del evento
			
, cualquier otro valor de entrada puede generar errores en el algoritmo.

* Una vez que ingresamos todo correctamente en el paso 3, le damos al botón ''Actualizar'' en (9) y podemos visualizar este nuevo dataframe en (10). 

*  Si se ven los datos de forma correcta y no deseamos hacer más ajustes, cambiamos en (9), el *Estado Análisis* de *Detenido* a *Iniciado* y estamos prontos para comenzar.

	
Es importante aclarar, que los datos ingresados deben ser todos númericos, incluso aquellos que representan variables categóricas (por ejemplo usar una variable binaria 0-1, para datos como el sexo de la persona).
