# Base de ejemplo para utilizar en la aplicación

**Dataset:** melanoma

**Librería en R:** MASS(Venables y Ripley 2002)

Este dataset surge de un estudio que se realiza en el período 1962-1977, e involucra a 205 pacientes diagnosticados con melanoma maligno, que fueron operados en el hospital universitario de Odense, en Dinamarca.

En el dataset que refiere al estudio, a cada paciente se le extirpó de forma completa el tumor mediante cirugía, además de extraer también 2.5 cm de la piel circundante. Entre las medidas
que se tomaron se encuentra el grosor de tumor y si presentaba úlceras o no. Estas son variables prónosticas importantes en el sentido en que los pacientes con un tumor de mayor tamaño y/o
ulcerado, presentan una mayor probabilidad de muerte por melanoma. El tiempo se presenta en meses desde el momento de la operación. A forma de resumen, se presentan las variables que
contiene el dataset:

- **id:** Numero de identificación del paciente.
- **time months:** El tiempo medido en meses, desde el momento de la operación hasta el tiempo que finaliza el estudio o que el paciente fallece.
- **status:** Variable binaria, que indica si el paciente finaliza el estudio o sale del mismo por motivos ajenos a la enfermedad (0), o si fallece (1).
- **sex:** Variable binaria, si es femenino (0) o masculino (1).
- **age:** Edad del paciente al momento de la operación.
- **year:** Año en que fue realizada la operación.
- **thickness:** Espesor del tumor en mm. También se le llama medición de Breslow.
- **ulcer:** Si presenta ulceración (1) o no la presenta (0). La ulceración es la ruptura de piel que se encuentra sobre el melanoma.
