coxmodelcond_cat <- function(times, status, varcat, h=10, kernel="epanechnikov",
                            type = "interior", cant_est=100, nombre_cat = 'Categoria'){
  
  cov_cats <- unique(varcat) %>% sort() 
  
  datos <- data.frame(times,status,varcat)
  
  # Creo un dataframe vacío para llenar con las estimaciones    
  df <- data.frame(Tiempo = numeric(), hazard = numeric() , Categoria = numeric()) 
  
  
  for (i in 1:length(cov_cats)) {
    
    temp <- subset(datos, varcat == cov_cats[i]) 
    
    set.seed(1234)
    
    fit_temp <- coxph(Surv(times, status) ~ varcat, data = temp)
    
    kh_temp <- khazard(times = temp$times, delta = temp$status, h = h, kernel = kernel,
                       type = type, t.length = cant_est)
    
    
    df <- rbind(df, data.frame(Tiempo = kh_temp$hazard[,1], 
                               hazard = kh_temp$hazard[,2],
                               Categoria = cov_cats[i]))
    
  }
  
  # Ajustes para el df final
  df$hazard <- df$hazard/max(df$hazard)
  df$Categoria <- as.factor(df$Categoria)
  
  c <- ggplot(df, aes(x =Tiempo ,y =hazard, color = Categoria)) + theme_bw() + 
    geom_line(alpha = 0.8, size=1) + scale_color_brewer(palette="Dark2") + 
    labs(color= nombre_cat) + labs(y = 'Hazard Rate')
  
  ggplotly(c)  
  
  
}
